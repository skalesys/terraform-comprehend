

![logo][logo]



[![pipeline status](https://gitlab.com/skalesys/terraform-comprehend/badges/master/pipeline.svg)](https://gitlab.com/skalesys/terraform-comprehend/commits/master)



 
# terraform-comprehend 


 
Terraform workspace that provision resources allowing telemetry and sentiment analysis with AWS Kinesis, S3 Bucket and AWS Comprehend services. 


---


## Screenshots


![amazon-kinesis-stream](docs/screenshots/0-amazon-kinesis-stream.png) |
|:--:|
| *Amazon Kinesis Stream* |


![amazon-kinesis-firehose](docs/screenshots/1-amazon-kinesis-firehose.png) |
|:--:|
| *Amazon Kinesis Firehose* |


![amazon-comprehend-sentiment-analysis](docs/screenshots/2-amazon-comprehend-sentiment-analysis.png) |
|:--:|
| *Amazon Comprehend Sentiment document analysis* |


![amazon-comprehend-sentiment-analysis-jobs](docs/screenshots/3-amazon-comprehend-sentiment-analysis-jobs.png) |
|:--:|
| *Amazon Comprehend Sentiment document analysis jobs* |


![amazon-comprehend-sentiment-analysis-result](docs/screenshots/4-amazon-comprehend-sentiment-analysis-result.png) |
|:--:|
| *Amazon Comprehend sentiment document analysis result* |


![amazon-comprehend-sentiment-analysis-bucket-result](docs/screenshots/5-amazon-comprehend-sentiment-analysis-bucket-result.png) |
|:--:|
| *Amazon Comprehend sentiment document analysis bucket result* |









## Requirements

In order to run this project you will need: 

- [Ubuntu][ubuntu] - Ubuntu is a complete Linux operating system, freely available with both community and professional support.
- [Terraform][terraform] - Write, Plan, and Create Infrastructure as Code




## Usage

```
make init install
make terraform/plan STAGE=development
make terraform/apply STAGE=development
```

How to upload the `samples` to S3:
```
aws s3 sync samples/ s3://sk-dev-analysis-stream/telemetry/
```

How to send sample data to Kinesis Stream:
```
make send/sample SAMPLE_NUMBER=1
```
It will the sample `samples/1.txt`.

Or to send all the samples:
```
make send/samples
```

How to list sentiment job detection via CLI:
```
aws comprehend list-sentiment-detection-jobs
```

How to described sentiment job detection via CLI:
```
aws comprehend describe-sentiment-detection-job --job-id d130ec18e33dea8ed149a23d3d08746e

Output:
{
    "SentimentDetectionJobProperties": {
        "InputDataConfig": {
            "S3Uri": "s3://sk-dev-analysis-stream/",
            "InputFormat": "ONE_DOC_PER_LINE"
        },
        "DataAccessRoleArn": "arn:aws:iam::096373988534:role/service-role/AmazonComprehendServiceRole-ReadInputOutputS3Buckets",
        "LanguageCode": "en",
        "JobStatus": "IN_PROGRESS",
        "JobName": "sk-dev-analysis-sentiment",
        "SubmitTime": 1557208458.464,
        "OutputDataConfig": {
            "S3Uri": "s3://sk-dev-analysis-stream-sentiment-result/096373988534-SENTIMENT-d130ec18e33dea8ed149a23d3d08746e/output/output.tar.gz"
        },
        "JobId": "d130ec18e33dea8ed149a23d3d08746e"
    }
}
```







## Makefile targets

```Available targets:

  analysis/start                     	Starts a sentiment analysis
  aws-nuke/install                   	Install aws-nuke
  base                               	Runs base playbook
  clean                              	Clean roots
  docker/install                     	Install docker
  gomplate/install                   	Install gomplate
  google-chrome/install              	Install google-chrome
  help/all                           	Display help for all targets
  help/all/plain                     	Display help for all targets
  help                               	Help screen
  help/short                         	This help short screen
  install                            	Install project requirements
  java/install                       	Install java
  openvpn/install                    	Install openvpn
  packer/install                     	Install packer
  packer/version                     	Prints the packer version
  pip/install                        	Install pip
  readme                             	Alias for readme/build
  readme/build                       	Create README.md by building it from README.yaml
  readme/install                     	Install README
  security/install                   	Install security
  spotify/install                    	Install spotify
  terraform/apply                    	Builds or changes infrastructure
  terraform/clean                    	Cleans Terraform vendor from Maker
  terraform/console                  	Interactive console for Terraform interpolations
  terraform/destroy                  	Destroy Terraform-managed infrastructure, removes .terraform and local state files
  terraform/fmt                      	Rewrites config files to canonical format
  terraform/get                      	Download and install modules for the configuration
  terraform/graph                    	Create a visual graph of Terraform resources
  terraform/init-backend             	Initialize a Terraform working directory with S3 as backend and DynamoDB for locking
  terraform/init                     	Initialize a Terraform working directory
  terraform/install                  	Install terraform
  terraform/output                   	Read an output from a state file
  terraform/plan                     	Generate and show an execution plan
  terraform/providers                	Prints a tree of the providers used in the configuration
  terraform/push                     	Upload this Terraform module to Atlas to run
  terraform/refresh                  	Update local state file against real resources
  terraform/show                     	Inspect Terraform state or plan
  terraform/taint                    	Manually mark a resource for recreation
  terraform/untaint                  	Manually unmark a resource as tainted
  terraform/validate                 	Validates the Terraform files
  terraform/version                  	Prints the Terraform version
  terraform/workspace                	Select workspace
  update                             	Updates roots
  vagrant/destroy                    	Stops and deletes all traces of the vagrant machine
  vagrant/install                    	Install vagrant
  vagrant/recreate                   	Destroy and creates the vagrant environment
  vagrant/update/boxes               	Updates all Vagrant boxes
  vagrant/up                         	Starts and provisions the vagrant environment
  version                            	Displays versions of many vendors installed
  virtualbox/install                 	Install virtualbox
  vlc/install                        	Install vlc
```








## References

For additional context, refer to some of these links. 

- [AWS Kinesis](https://aws.amazon.com/kinesis/) - Amazon Kinesis makes it easy to collect, process, and analyze real-time, streaming data so you can get timely insights and react quickly to new information.
- [AWS S3](https://aws.amazon.com/s3/) - Amazon Simple Storage Service (Amazon S3) is an object storage service that offers industry-leading scalability, data availability, security, and performance. 
- [AWS Comprehend](https://aws.amazon.com/comprehend/) - Amazon Comprehend is a natural language processing (NLP) service that uses machine learning to find insights and relationships in text. No machine learning experience required.




## Resources

Resources used to create this project: 

- [Photo](https://unsplash.com/photos/ORYtC6R8omE) - by Dean Truderung on Unsplash
- [Gitignore.io](https://gitignore.io) - Defining the `.gitignore`
- [LunaPic](https://www341.lunapic.com/editor/) - Image editor (used to create the avatar)





## Repository

We use [SemVer](http://semver.org/) for versioning. 

- **[Branches][branches]**
- **[Commits][commits]**
- **[Tags][tags]**
- **[Contributors][contributors]**
- **[Graph][graph]**
- **[Charts][charts]**









## Contributors

Thank you so much for making this project possible: 

- [Valter Silva](https://gitlab.com/valter-silva)



## Copyright

Copyright © 2019-2019 [SkaleSys][company]





[logo]: docs/logo.jpeg


[company]: https://skalesys.com
[contact]: https://skalesys.com/contact
[services]: https://skalesys.com/services
[industries]: https://skalesys.com/industries
[training]: https://skalesys.com/training
[insights]: https://skalesys.com/insights
[about]: https://skalesys.com/about
[join]: https://skalesys.com/Join-Our-Team

[ansible]: https://ansible.com
[terraform]: http://terraform.io
[packer]: https://www.packer.io
[docker]: https://www.docker.com/
[vagrant]: https://www.vagrantup.com/
[kubernetes]: https://kubernetes.io/
[spinnaker]: https://www.spinnaker.io/
[jenkins]: https://jenkins.io/
[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/




[aws]: https://aws.amazon.com/






[branches]: https://gitlab.com/skalesys/terraform-comprehend/branches
[commits]: https://gitlab.com/skalesys/terraform-comprehend/commits
[tags]: https://gitlab.com/skalesys/terraform-comprehend/tags
[contributors]: https://gitlab.com/skalesys/terraform-comprehend/graphs
[graph]: https://gitlab.com/skalesys/terraform-comprehend/network
[charts]: https://gitlab.com/skalesys/terraform-comprehend/charts


